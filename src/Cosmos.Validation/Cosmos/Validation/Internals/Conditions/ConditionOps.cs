﻿namespace Cosmos.Validation.Internals.Conditions
{
    public enum ConditionOps : int
    {
        Break = 0,
        And = 1,
        Or = 2,
    }
}